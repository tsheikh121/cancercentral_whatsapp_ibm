package uk.org.cancercentral.sms.controller;

import com.twilio.twiml.MessagingResponse;
import com.twilio.twiml.messaging.Body;
import com.twilio.twiml.messaging.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import uk.org.cancercentral.sms.dto.CancerCentralRequest;
import uk.org.cancercentral.sms.dto.CancerCentralResponse;
import uk.org.cancercentral.sms.services.CancerCentralService;
import uk.org.cancercentral.sms.services.SmsService;

import java.util.List;
import java.util.Map;

@RestController
public class WatsonController {

    @Autowired
    private CancerCentralService cancerCentralService;
    @Autowired
    private SmsService smsService;

    @PostMapping(value = "/sms", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
    public String respondToTwilio(@RequestParam Map<String, Object> values) {

        CancerCentralRequest cancerCentralRequest = new CancerCentralRequest();
        cancerCentralRequest.setQuestion((String) values.get("Body"));

        CancerCentralResponse cancerCentralResponse = cancerCentralService.getCancerCentralResponse((String) values.get("From"), cancerCentralRequest);

        List<String> watsonOutput = cancerCentralResponse.getWatsonResponse().getOutput().getText();

        return smsService.getTwimlResponse(watsonOutput, cancerCentralResponse.getLocations()).toXml();
    }

}
