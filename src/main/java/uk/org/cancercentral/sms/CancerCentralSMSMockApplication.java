package uk.org.cancercentral.sms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CancerCentralSMSMockApplication {
    public static void main(String[] args) {
        SpringApplication.run(CancerCentralSMSMockApplication.class, args);
    }
}
