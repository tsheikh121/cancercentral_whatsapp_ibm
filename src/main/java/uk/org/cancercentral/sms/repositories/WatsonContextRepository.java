package uk.org.cancercentral.sms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import uk.org.cancercentral.sms.dto.WatsonContext;

@Repository
public interface WatsonContextRepository extends CrudRepository< WatsonContext, String> {
    WatsonContext findByNumber(String number);
}
