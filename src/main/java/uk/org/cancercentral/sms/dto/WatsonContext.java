package uk.org.cancercentral.sms.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class WatsonContext {
    @Id
    private String number;
    @Column(columnDefinition = "TEXT")
    private String watsonContext;

    public WatsonContext(String number, String watsonContext) {
        this.number = number;
        this.watsonContext = watsonContext;
    }

    public WatsonContext() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getWatsonContext() {
        return watsonContext;
    }

    public void setWatsonContext(String watsonContext) {
        this.watsonContext = watsonContext;
    }
}
