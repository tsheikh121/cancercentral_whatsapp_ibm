package uk.org.cancercentral.sms.dto;

import com.ibm.watson.developer_cloud.assistant.v1.model.MessageResponse;

import java.util.List;

public class CancerCentralResponse {
    public MessageResponse watsonResponse;
    public List<CancerCentralLocation> locations;

    public CancerCentralResponse(MessageResponse watsonResponse, List<CancerCentralLocation> locations) {
        this.watsonResponse = watsonResponse;
        this.locations = locations;
    }

    public MessageResponse getWatsonResponse() {
        return watsonResponse;
    }

    public void setWatsonResponse(MessageResponse watsonResponse) {
        this.watsonResponse = watsonResponse;
    }

    public List<CancerCentralLocation> getLocations() {
        return locations;
    }

    public void setLocations(List<CancerCentralLocation> locations) {
        this.locations = locations;
    }
}
