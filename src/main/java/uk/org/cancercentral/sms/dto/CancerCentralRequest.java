package uk.org.cancercentral.sms.dto;

import com.ibm.watson.developer_cloud.assistant.v1.model.Context;
import com.ibm.watson.developer_cloud.assistant.v2.model.MessageContext;

public class CancerCentralRequest {
    public String question;
    public Context context;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Context getContext() {
        return context;
    }

    public void setMessageContext(Context context) {
        this.context = context;
    }
}
