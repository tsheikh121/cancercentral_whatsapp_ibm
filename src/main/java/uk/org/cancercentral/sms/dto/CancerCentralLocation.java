package uk.org.cancercentral.sms.dto;

public class CancerCentralLocation {
    private String website;
    private String number;
    private String address;
    private String name;

    public CancerCentralLocation(String website, String number, String address, String name) {
        this.website = website;
        this.number = number;
        this.address = address;
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
