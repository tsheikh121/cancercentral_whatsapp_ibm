package uk.org.cancercentral.sms.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.watson.developer_cloud.assistant.v1.model.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.org.cancercentral.sms.dto.WatsonContext;
import uk.org.cancercentral.sms.repositories.WatsonContextRepository;

import java.io.IOException;

@Service
public class ContextService {

    @Autowired
    private WatsonContextRepository watsonContextRepository;

    @Autowired
    private ObjectMapper objectMapper;

    public Context getContext(String number) {
        WatsonContext watsonContext = watsonContextRepository.findByNumber(number);

        Context marshaledContext = null;

        try {
            if(watsonContext!= null) {
                marshaledContext = objectMapper.readValue(watsonContext.getWatsonContext(), Context.class);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return  marshaledContext;
    }

    public void saveContext(String number, Context context) {
        try {
            watsonContextRepository.save(new WatsonContext(number, objectMapper.writeValueAsString(context)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
