package uk.org.cancercentral.sms.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.watson.developer_cloud.assistant.v1.Assistant;
import com.ibm.watson.developer_cloud.assistant.v1.model.Context;
import com.ibm.watson.developer_cloud.assistant.v1.model.InputData;
import com.ibm.watson.developer_cloud.assistant.v1.model.MessageOptions;
import com.ibm.watson.developer_cloud.assistant.v1.model.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.org.cancercentral.sms.dto.CancerCentralLocation;
import uk.org.cancercentral.sms.dto.CancerCentralRequest;
import uk.org.cancercentral.sms.dto.CancerCentralResponse;
import uk.org.cancercentral.sms.dto.WatsonContext;
import uk.org.cancercentral.sms.repositories.WatsonContextRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CancerCentralService {

    @Autowired
    private WatsonContextRepository contextRepository;

    @Autowired
    private ContextService contextService;

    @Autowired
    private ObjectMapper objectMapper;



    Assistant service = new Assistant("2018-09-20");

    private final String WATSON_WORKSPACE_ID =  ""  //insert ID heere;
    private final String WATSON_API_TOKEN = "";  //insert Token here

    public CancerCentralService() {
        service.setUsernameAndPassword("apikey", WATSON_API_TOKEN);
        service.setEndPoint("https://gateway-lon.watsonplatform.net/assistant/api");
    }

    public CancerCentralResponse getCancerCentralResponse(String from, CancerCentralRequest cancerCentralRequest) {

        Context context = contextService.getContext(from);
        InputData input = new InputData.Builder(cancerCentralRequest.getQuestion()).build();

        MessageOptions options = new MessageOptions.Builder(WATSON_WORKSPACE_ID)
                .input(input)
                .context(context)
                .build();

        MessageResponse response = service.message(options).execute();
        contextService.saveContext(from, response.getContext());

        List<CancerCentralLocation> locations = new ArrayList<>();

        for (String string : response.getOutput().getText()) {

            if(string !=  null && string.contains("I will look")) {
                locations.add(new CancerCentralLocation("http://example.com", "07749785109", "1 Inifinite Loop", "CancerCharity"));
                locations.add(new CancerCentralLocation("http://cancer.com", "24312561241", "That place, Some Country", "A Charity"));
                break;
            }
        }

        CancerCentralResponse cancerCentralResponse = new CancerCentralResponse(response, locations);

        return cancerCentralResponse;
    }
}
