package uk.org.cancercentral.sms.services;

import com.twilio.twiml.MessagingResponse;
import com.twilio.twiml.TwiML;
import com.twilio.twiml.messaging.Body;
import com.twilio.twiml.messaging.Message;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;
import uk.org.cancercentral.sms.dto.CancerCentralLocation;

import java.util.List;

@Service
public class SmsService {

    public TwiML getTwimlResponse(List<String> responseText, List<CancerCentralLocation> locations) {
        MessagingResponse twiml = new MessagingResponse
                .Builder()
                .build();

        for (String response : responseText) {
            if(response == null) {
                continue;
            }
            String sanitizedText = Jsoup.parse(response).wholeText();
            twiml.getChildren().add(getMessage(sanitizedText));
        }

        for (CancerCentralLocation cancerCentralLocation : locations) {

            StringBuilder stringBuilder = new StringBuilder(cancerCentralLocation.getName());
            stringBuilder.append("\n");
            stringBuilder.append("Address: " + cancerCentralLocation.getAddress());
            stringBuilder.append("\n");
            stringBuilder.append("Number: " + cancerCentralLocation.getNumber());
            twiml.getChildren().add(getMessage(stringBuilder.toString()));
        }

        return twiml;
    }

    private Message getMessage(String content) {
        Body smsBody = new Body
                .Builder(content)
                .build();
        return new Message
                .Builder()
                .body(smsBody)
                .build();
    }
}
